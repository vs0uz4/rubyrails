p 1.9.class
p 1.0.class
p
p 1_000_000.class
p (1000000 * 1000000000000000).class

p 65 + 10
p 2 * 10

p 65.chr
p 1.even?
p 2.odd?
p 2.6.round
p 22.succ

3.times { print "Developers " }

1.upto(5) { |i| puts i }
5.downto(1) { |i| puts i }
