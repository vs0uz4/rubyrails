puts 1..10 # => 1, 2, 3 ... 10
puts (1..10).to_a.inspect
puts

puts "A".."Z" # => "A", "B", "C" ... "Z"
puts ("A".."Z").to_a.inspect
puts

(1..10).each { |i| print "#{i} " }
puts
5.upto(10) { |i| print "#{i} " }
puts

