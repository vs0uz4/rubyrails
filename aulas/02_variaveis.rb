# Variáveis são referencias a objetos
a = 1
puts a.class 		# => FixNum

a = "123"
puts a.class		# => String

b = a
puts "ID de B:#{b.object_id} mas o ID de A também é:#{a.object_id}"

# Também existem pseudo variáveis
c = false
d = false
puts "ID de C:#{c.object_id} e D:#{d.object_id}, e ambos #{c.class}"

# Constantes
INSTRUTOR = "daniel"

# Globais [OBS - Não são muito usadas]
$curso = "Rails"

# Globais pré-definidas
puts "constantes globais pré-definidas: #{RUBY_VERSION}, #{ENV}, #{RUBY_PLATFORM}"

# Variáveis pré-definidas
puts "ID do processo:#{$$}; programa em execução:#{$0}"
