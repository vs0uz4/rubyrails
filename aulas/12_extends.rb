valor = 20.62

puts "Float responde a to_money?"
puts valor.respond_to?(:to_money)

class Float
  def to_money(currency="$")
    "#{currency} #{self.to_s}"
  end
end

puts "Float responde a to_money?"
puts valor.respond_to?(:to_money)
puts valor.to_money
