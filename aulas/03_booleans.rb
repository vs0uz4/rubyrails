puts true
puts false

puts true.class # => TrueClass
puts false.class # => FalseClass

#  nil não é false
puts nil == false
puts nil && true

nome = nil
if nome == nil
  puts "PREENCHA O NOME" 
end

# ESCREVA CÓDIGO EXPRESSIVO
puts "PREENCHA O NOME" if nome.nil?
puts "PREENCHA O NOME" unless nome

object1 = Object.new
object2 = Object.new

puts object1 && object2
puts object1 || object2

# REFLEXÃO
puts object1 && String.respond_to?(:upto)

ruby = nil
ruby ||= "Yukihiro Matsumoto"
puts ruby

rails = "David Heinemeier Hansson"
rails ||= "DHH"
puts rails

# MÉTODOS BOOLEANOS COM ?
def ruby_developer?
  true
end

p "really smart" if ruby_developer?
