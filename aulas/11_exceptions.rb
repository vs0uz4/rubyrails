# TRATAMENTO DE EXCESSOES
begin
  c = 1 / 0
  puts c
rescue ZeroDivisionError => ex
  puts "Erro: " + ex.message
end
 
# TRATAMENTO PARA VARIAS EXCESSOES
begin
  c = 1 / gets
  puts c
rescue ZeroDivisionError
  puts "Divisão por zero"
rescue => ex
  puts "Outro erro: #{ex}"
end
 
# ELSE
begin
  c = 1 / gets
rescue
  puts "Um erro ocorreu"
else
  puts c
end
 
# ENSURE
begin
  c = 1/gets
  puts c
rescue ZeroDivisionError => ex
  puts "Divisão por zero #{ex}"
rescue IOError, StandardError
  puts "Outro erro"
ensure
  puts "Sempre executado"
end

# FORMA ABREVIADA EM METODOS
def teste(value)
  c = 1/value
rescue
  puts "Outro erro"
end

puts teste("sopa")

