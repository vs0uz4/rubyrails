# Array

names = ["Daniel", "João", "Marcos", "Alex"]
p names.class

p names.last
p names.first
p names[2]

names.each {|n| puts n }

names << "Junio"
p names

names += ['Tadeu', 'Felipe']
p names

names = %w(Daniel João Marcos Alex)
p names
