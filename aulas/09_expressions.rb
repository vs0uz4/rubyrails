a = b = c = 0

1.class.superclass

value = 10
rating = if value < 3
          "Bad"
         else
          "Good"
         end

p rating

name = "Ruby"

result = case name
         when "Ruby"
           "language"
         when "Rails"
           "framework"
         end

p result
