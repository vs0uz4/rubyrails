p "Yukhiro Matz".class
p 'Yukhiro Matz'.class

name = "Yukhiro"
last = "Matz"
p name + " or just " + "Matz"

p "#{name} or just #{last}"
p "#{name} or just #{last unless last.nil?}"
p '#{name} or just #{last}'

p "Ruby originated in Japan during the mid-1990s was designed by \"Matz\"."
p %{Ruby originated in Japan during the mid-1990s was designed by "Matz".}

p <<-EOF
  She was more like a beauty queen From a movie scene
  I said: Don't mind, but what do you mean I am the one

  Gonna dance on the floor in the round She said I am the one
  Gonna dance on the floor in the round She told me her name was Billie Jean

  As she caused a scene Then every head turned with eyes 
  That dreamed of being the one Gonna dance on the floor in the round
EOF

music = "billie jean"
puts music.capitalize
puts music
puts music.capitalize!
puts music
puts music.upcase

puts "A".next
puts "ABC".length # => 3
puts "123"[0]     # => 1
puts "123"[0].chr # => 1
puts "ABC"[0].chr # => "A"
puts "ABC"[0, 1]  # => "A"
