# Em Ruby tudo é Objeto (Object)
puts 1.class 		# => Fixnun
puts "123".class  	# => String
puts false.class  	# => FalseClass

# Uma forma de Criar Classes
class Carro
end

puts Carro.new.class  	# => Carro

# Outra forma de Criar Classes
Moto = Class.new
honda = Moto.new

puts honda		# => Instância de Moto
puts honda.class	# => Moto
