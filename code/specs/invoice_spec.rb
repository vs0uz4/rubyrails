require File.expand_path("helper")
require 'tmpdir'

describe Invoice do
  def invoice(params = {})
    @invoice ||= make_invoice(params)
  end

  describe 'default' do
    it 'has R$ as default currency' do
      invoice.currency = nil
      assert_equal invoice.currency, "R$"
    end

    it 'has 0.06 as default tax' do
      assert_equal invoice.tax, 0.06
    end

    it 'has 0 as default discount' do
      assert_equal invoice.discount, 0
    end
  end
  
  describe 'services' do
    it 'receives a bunch of services' do
      invoice.services = [Service.new, Service.new, Service.new]
      assert_equal invoice.services.length, 3
    end 
  end   

  describe 'customer' do
    it 'has customer name' do
      invoice.customer = Customer.new("eGenial")
      assert_equal invoice.customer_name, "eGenial"
    end
  end

  describe 'owner' do
    it 'has owner name' do
      invoice.owner = Owner.new("eGenial")
      assert_equal invoice.owner_name, "eGenial"
    end
  end

  describe 'invoice amount calculation' do
    it 'has a subtotal without taxes and discounts' do
      assert_equal 10, invoice.subtotal
    end
    
    it 'should have an amount with taxes' do
      assert_equal 10.6, invoice.amount
    end
  
    it 'should apply discounts when available' do
      invoice.discount = 1
      assert_equal 9.6, invoice.amount
    end
  end

  describe 'pdf creation' do
    it 'create a pdf for the invoice' do
      customer = Customer.new("Mola Mestra")
      address = Address.new
      address.street  = "Rua Paulo da Silva, 68"
      address.city    = "Rio de Janeiro"
      address.country = "Brasil"
      address.phone   = "21 2404-7825"
      address.postal_code = "21831-200"
      customer.address = address
      invoice.customer = customer      
      
      owner = Owner.new("Daniel Lopes")
      address = Address.new()
      address.street  = "Rua Paulo da Silva, 102"
      address.city    = "Rio de Janeiro"
      address.country = "Brasil"
      address.phone   = "21 3404-7825"
      address.postal_code = "21831-201"
      owner.address = address
      invoice.owner = owner
      
      Dir.mktmpdir do |dir|
        #invoice.to_pdf("/home/vsouza/Desktop")
        invoice.to_pdf(dir)
        #expected = "/home/vsouza/Desktop/1-mola-mestra.pdf"
        expected = "#{dir}/1-mola-mestra.pdf"
        assert File.file?(expected)
      end
    end 
  end
end
