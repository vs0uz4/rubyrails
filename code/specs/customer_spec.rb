require './helper.rb'

describe Customer do

  def customer
    @customer ||= Customer.new("Daniel Lopes")
  end

  it 'has a name' do
    assert_equal customer.name, "Daniel Lopes"
  end
  
  describe 'address' do
  
    it 'should delegate address attributes to customer address' do
      params = {
        city: "Belo Horizonte",
        country: "Brasil",
        phone: "31 30774566",
        street: "Alameda Apiranga",
        postal_code: "30434343" 
      }
 
      customer.address = Address.new(params)
      assert_equal customer.city, params[:city]
      assert_equal customer.country, params[:country]
      assert_equal customer.phone, params[:phone]
      assert_equal customer.street, params[:street]
      assert_equal customer.postal_code, params[:postal_code]

#     Método menos explícito para testar os parametros, mais faz a mesma função do código acima
#
#      params.each do |key, value|
#        expected = @customer.send(key)
#        assert_equal expected, value
#      end

    end
  end
end
