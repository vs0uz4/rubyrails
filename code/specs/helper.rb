$LOAD_PATH.unshift(File.dirname(__FILE__))

require 'minitest/spec'
require 'minitest/autorun'

Dir["#{File.dirname(__FILE__)}/../lib/**/*.rb"].each {|f| require f}

def make_invoice(params = {})
  @invoice ||= begin
    defaults = {  id: 1, issue_date: Date.today, due_date: Date.today + 2 }
    invoice = Invoice.new(defaults.merge(params))
  
    services = 5.times.collect do |i|
      s = Service.new
      s.type = "System #{i}"
      s.quantity = 1
      s.unit_price = 2
      s
    end
    
    invoice.services = services
    invoice
  end
end
