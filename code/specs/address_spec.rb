require './helper.rb'

describe Address do

  def address
    @address ||= begin
      a = Address.new
      a.street = "Tenente Brito Melo"
      a.city = "Belo Horizonte"
      a.country = "Brasil"
      a.postal_code = "3434343"
      a.phone = "3125456721"
      a
    end
  end

  it 'should know how to print him self as string' do
    assert_equal address.to_s, "Tenente Brito Melo, Belo Horizonte - Brasil, 3434343, 3125456721"
  end

  it 'is complete with all field filled' do
    assert address.complete?
  end

  it 'is incomplete with one field empty' do
    address.city = nil
    assert address.incomplete?
  end

end
