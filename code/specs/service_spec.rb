require './helper'

describe Service do
  
  it 'should have 1 as default for quantity' do
    assert_equal Service.new.quantity, 1
  end
  
  it 'has his amount as the quantity multiplied by unit price' do
    service = Service.new
    service.unit_price = 20
    service.quantity = 2

    assert_equal service.amount, 40
  end

end
